package com.pencil;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.pencil.adapter.CustomAdapter;


public class Order extends AppCompatActivity {

    private ListView list_item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        list_item = (ListView) findViewById(R.id.listdetails);

        int sr[] = {1, 2, 3};
        String[] itemname = {"book", "pen", "pencil"};
        int[] quantity = {0, 0, 0};
        int[] price = {20, 30, 5};


        CustomAdapter ca = new CustomAdapter(this, sr, itemname, price, quantity);
        list_item.setAdapter(ca);
    }
}
