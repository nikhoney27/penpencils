package com.pencil.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.pencil.R;

/**
 * Created by Nikhil Sharma on 3/16/2017.
 */

public class CustomAdapter extends BaseAdapter {
    Activity a;
    int sr[];
    int quantity[];

    String itemname[];
    int price[];

    int bookCount = 0;
    int penCount = 0;
    int pencilCount = 0;

    int bookPrice = 0;
    int penPrice = 0;
    int pencilPrice = 0;


    private String data, pricedata;


    public CustomAdapter(Activity a, int[] sr, String[] itemname, int[] price, int quantity[]) {
        this.a = a;
        this.sr = sr;
        this.itemname = itemname;
        this.price = price;
        this.quantity = quantity;

    }

    @Override
    public int getCount() {
        return itemname.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class viewHolder {

        TextView srh, item, qt, rate;
        Button plus, minus;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final viewHolder viewHolder = new viewHolder();
        LayoutInflater li = a.getLayoutInflater();
        View view = li.inflate(R.layout.customlist, parent, false);
        viewHolder.srh = (TextView) view.findViewById(R.id.s_no);
        viewHolder.item = (TextView) view.findViewById(R.id.i_name);
        viewHolder.qt = (TextView) view.findViewById(R.id.qty);
        viewHolder.plus = (Button) view.findViewById(R.id.pl);
        viewHolder.minus = (Button) view.findViewById(R.id.min);
        viewHolder.rate = (TextView) view.findViewById(R.id.pr);


        viewHolder.srh.setText(Integer.toString(sr[position]));
        viewHolder.item.setText(itemname[position]);
        viewHolder.qt.setText(Integer.toString(quantity[position]));

        viewHolder.rate.setText(Integer.toString(0));


        viewHolder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (position) {
                    case 0:
                        if (bookCount >= 0) {
                            bookCount++;
                            bookPrice = bookPrice + 20;
                            viewHolder.qt.setText(String.valueOf(bookCount));
                            viewHolder.rate.setText(String.valueOf(bookPrice));
                        }
                        break;
                    case 1:
                        if (penCount >= 0) {
                            penCount++;
                            penPrice += 30;
                            viewHolder.qt.setText(String.valueOf(penCount));
                            viewHolder.rate.setText(String.valueOf(penPrice));
                        }
                        break;
                    case 2:
                        if (pencilCount >= 0) {
                            pencilCount++;
                            pencilPrice += 5;
                            viewHolder.qt.setText(String.valueOf(pencilCount));
                            viewHolder.rate.setText(String.valueOf(pencilPrice));
                        }
                        break;
                }


            }
        });
        viewHolder.minus.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {

                switch (position) {
                    case 0:
                        if (bookCount > 0) {
                            bookCount--;
                            bookPrice -= 20;
                            viewHolder.qt.setText(String.valueOf(bookCount));
                            viewHolder.rate.setText(String.valueOf(bookPrice));
                        }
                        break;
                    case 1:
                        if (penCount > 0) {
                            penCount--;
                            penPrice -= 30;
                            viewHolder.qt.setText(String.valueOf(penCount));
                            viewHolder.rate.setText(String.valueOf(penPrice));
                        }
                        break;
                    case 2:
                        if (pencilCount > 0) {
                            pencilCount--;
                            pencilPrice -= 5;
                            viewHolder.qt.setText(String.valueOf(pencilCount));
                            viewHolder.rate.setText(String.valueOf(pencilPrice));
                        }
                        break;
                }

            }
        });

        return view;
    }
}
